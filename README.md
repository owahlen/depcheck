depcheck
========
Gradle plugin that helps developers to avoid version conflicts between artifacts and to clean up the project dependencies.

package com.infinit.gradle.depcheck

import ch.qos.logback.classic.Level
import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.internal.Node
import com.infinit.gradle.depcheck.testutils.DependencyGraphTestUtils
import com.infinit.gradle.depcheck.testutils.LoggerUtils
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

@Mixin([DependencyGraphTestUtils, LoggerUtils])
class DepcheckConflictsTaskSpec extends Specification {

    def "can add task to project"() {
        setup:
        Project project = ProjectBuilder.builder().build()

        when:
        Task task = project.task('depcheckConflicts', type: DepcheckConflictsTask)

        then:
        task instanceof DepcheckConflictsTask
    }

    def "check for conflicting artifact versions"() {
        setup:
        // setup project
        Project project = ProjectBuilder.builder().build()
        DepcheckPluginExtension extension = (DepcheckPluginExtension) project.extensions.create(DepcheckPlugin.DEPCHECK_PLUGIN_EXTENSION, DepcheckPluginExtension)
        // setup dependency graph
        def graphDefinition = {
            projectConfiguration("project", "config1") {
                node("group1:name1:1.0.0")
                node("group2:name1:1.0.0")
                node("group2:name1:1.0.1")
                edge("group1:name1:1.0.0", "group2:name1:1.0.0")
                edge("group1:name1:1.0.0", "group2:name1:1.0.1")
            }
        }
        DependencyGraph dependencyGraph = createDependencyGraph(graphDefinition)

        // mock static methods in DepcheckUtils:
        GroovyMock(DepcheckUtils, global: true)
        DepcheckUtils.getOrCreateDependencyGraph(_) >> dependencyGraph
        DepcheckUtils.getExtension(_) >> extension

        // create DepcheckConfigTask and capture logs
        DepcheckConflictsTask task = (DepcheckConflictsTask) project.task('depcheckConflicts', type: DepcheckConflictsTask)
        String expectedResult = "[ERROR Version conflict found for module 'group2:name1':]" +
                "[ERROR   'group2:name1:1.0.0' referenced from:]" +
                "[ERROR     'group1:name1:1.0.0']" +
                "[ERROR   'group2:name1:1.0.1' referenced from:]" +
                "[ERROR     'group1:name1:1.0.0']"
        attachTestAppender(Level.WARN)

        when:
        task.depcheckConflicts()

        then:
        thrown(GradleException)
        logAsString == expectedResult

        cleanup:
        detachTestAppender()
    }

    def "find conflicting module values"() {
        setup:
        Project project = ProjectBuilder.builder().build()
        DepcheckPluginExtension extension = project.extensions.create(DepcheckPlugin.DEPCHECK_PLUGIN_EXTENSION, DepcheckPluginExtension)
        DepcheckConflictsTask task = (DepcheckConflictsTask) project.task('depcheckConflicts', type: DepcheckConflictsTask)
        extension.conflictingVersionRegexp = /^(\d+\.\d+)/
        Collection<List<Node>> moduleValues = [
                [
                        // not a conflict for given conflictingVersionRegexp
                        new Node(group: 'group1', name: 'name1', version: '1.0.0'),
                        new Node(group: 'group1', name: 'name1', version: '1.0.1')
                ],
                [
                        // conflict for given conflictingVersionRegexp
                        new Node(group: 'group1', name: 'name2', version: '1.0.0'),
                        new Node(group: 'group1', name: 'name2', version: '1.1.0')
                ]
        ]

        when:
        Collection<List<Node>> conflictingModuleValues = task.findConflictingModuleValues(moduleValues)

        then:
        conflictingModuleValues as Set<List<Node>> == [
                [
                        new Node(group: 'group1', name: 'name2', version: '1.0.0'),
                        new Node(group: 'group1', name: 'name2', version: '1.1.0')
                ]
        ] as Set<List<Node>>
    }

    def "extract conflicting version substring"() {
        setup:
        Project project = ProjectBuilder.builder().build()
        DepcheckPluginExtension extension = project.extensions.create(DepcheckPlugin.DEPCHECK_PLUGIN_EXTENSION, DepcheckPluginExtension)
        DepcheckConflictsTask task = (DepcheckConflictsTask) project.task('depcheckConflicts', type: DepcheckConflictsTask)

        when:
        extension.conflictingVersionRegexp = regexp

        then:
        task.getConflictingVersionSubstring(version) == expected

        where:
        regexp             | version    | expected
        null               | '1.2.3'    | '1.2.3'   // no regexp
        ''                 | '1.2.3'    | '1.2.3'   // no regexp
        /^(\d+\.\d+)/      | '1.2.3'    | '1.2'     // match first two version digits
        /^(\d+\.\d+)/      | '11.22.33' | '11.22'   // match with several decimal digits
        /^(\d+\.\d+)\.\d+/ | '1.2.3'    | '1.2'     // match of substring of the version
        /^(\d+\-\d+)/      | '1.2.3'    | '1.2.3'   // not matching regexp
    }

}

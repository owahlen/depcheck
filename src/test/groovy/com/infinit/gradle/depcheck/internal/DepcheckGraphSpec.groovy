package com.infinit.gradle.depcheck.internal

import com.infinit.gradle.depcheck.testutils.DependencyGraphTestUtils
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.ModuleVersionIdentifier
import org.gradle.api.artifacts.ModuleVersionSelector
import org.gradle.api.artifacts.ResolvableDependencies
import org.gradle.api.artifacts.result.ModuleVersionSelectionReason
import org.gradle.api.artifacts.result.ResolutionResult
import org.gradle.api.artifacts.result.ResolvedDependencyResult
import org.gradle.api.artifacts.result.ResolvedModuleVersionResult
import org.gradle.api.internal.artifacts.configurations.DefaultConfigurationContainer
import spock.lang.Specification

@Mixin(DependencyGraphTestUtils)
class DepcheckGraphSpec extends Specification {

    def "adding nodes and edges to a cyclic DependencyGraph"() {
        setup:
        def graphDefinition = {
            projectConfiguration("project", "config1") {
                node("group1:name1:1.0.0")
                node("group2:name1:1.0.0")
                node("group2:name2:1.0.0")
                edge("group1:name1:1.0.0", "group2:name1:1.0.0")
                edge("group1:name1:1.0.0", "group2:name2:1.0.0")
                edge("group2:name2:1.0.0", "group1:name1:1.0.0")
            }
        }

        when:
        DependencyGraph dependencyGraph = createDependencyGraph(graphDefinition)

        then:
        validateDependencyGraph(dependencyGraph)
    }

    def "filtering of configurations using configurationRegexp"() {
        setup:
        ResolvedModuleVersionResult rootMock1 = Mock(ResolvedModuleVersionResult) {
            getId() >> Mock(ModuleVersionIdentifier) {
                getGroup() >> 'group1'
                getName() >> 'name1'
                getVersion() >> '1.0.0'
            }
            getDependencies() >> []
        }
        ResolvedModuleVersionResult rootMock2 = Mock(ResolvedModuleVersionResult) {
            getId() >> Mock(ModuleVersionIdentifier) {
                getGroup() >> 'group2'
                getName() >> 'name2'
                getVersion() >> '1.0.0'
            }
            getDependencies() >> []
        }
        List<Configuration> configurationsMock = [
                Mock(Configuration) {
                    getName() >> "config1"
                    copyRecursive() >> Mock(Configuration) {
                        getIncoming() >> Mock(ResolvableDependencies) {
                            getResolutionResult() >> Mock(ResolutionResult) {
                                getRoot() >> rootMock1
                            }
                        }
                    }
                },
                Mock(Configuration) {
                    getName() >> "config2"
                    copyRecursive() >> Mock(Configuration) {
                        getIncoming() >> Mock(ResolvableDependencies) {
                            getResolutionResult() >> Mock(ResolutionResult) {
                                getRoot() >> rootMock2
                            }
                        }
                    }
                }
        ]
        // the ConfigurationsContainer is a spy since an implementation of Set is needed for iteration over all elements
        ConfigurationContainer configurationContainerSpy =
            Spy(DefaultConfigurationContainer, constructorArgs: [null, null, null, null, null])
        configurationContainerSpy.addAll(configurationsMock)
        Project projectMock = Mock(Project) {
            getAllprojects() >> [
                    Mock(Project) {
                        getName() >> "project"
                        getConfigurations() >> configurationContainerSpy
                    }
            ]
        }

        // create mock for the gradle root project
        Project rootProject = projectMock

        // create empty DependencyGraph with a regexp
        DependencyGraph dependencyGraph = new DependencyGraph(rootProject: rootProject, configurationRegexp: 'config1')

        when:
        dependencyGraph.populate()

        then:
        dependencyGraph.moduleVersions.values() as Set<Node> == [new Node(group: 'group1', name: 'name1', version: '1.0.0')] as Set<Node>
    }

    def "population of a cyclic DependencyGraph with one project configuration"() {
        setup:
        // construct mock for gradle resolver results including the dependencies
        ResolvedModuleVersionResult rootMock = Mock(ResolvedModuleVersionResult) {
            getId() >> Mock(ModuleVersionIdentifier) {
                getGroup() >> 'group1'
                getName() >> 'name1'
                getVersion() >> '1.0.0'
            }
            getDependencies() >> [
                    Mock(ResolvedDependencyResult) {
                        getRequested() >> Mock(ModuleVersionSelector) {
                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                        }
                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                            getId() >> Mock(ModuleVersionIdentifier) {
                                getGroup() >> 'group2'
                                getName() >> 'name1'
                                getVersion() >> '1.0.0'
                            }
                            getDependencies() >> []
                        }
                    },
                    Mock(ResolvedDependencyResult) {
                        getRequested() >> Mock(ModuleVersionSelector) {
                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                        }
                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                            getId() >> Mock(ModuleVersionIdentifier) {
                                getGroup() >> 'group2'
                                getName() >> 'name2'
                                getVersion() >> '1.0.0'
                            }
                            getDependencies() >> [
                                    Mock(ResolvedDependencyResult) {
                                        getRequested() >> Mock(ModuleVersionSelector) {
                                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                                        }
                                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                                            getId() >> Mock(ModuleVersionIdentifier) {
                                                getGroup() >> 'group1'
                                                getName() >> 'name1'
                                                getVersion() >> '1.0.0'
                                            }
                                            getDependencies() >> []
                                        }
                                    }
                            ]
                        }
                    }
            ]
        }

        // create mock for the gradle root project
        Project rootProject = createProjectMock(rootMock)

        // create empty DependencyGraph with a regexp
        DependencyGraph dependencyGraph = new DependencyGraph(rootProject: rootProject)

        when:
        dependencyGraph.populate()

        then:
        validateDependencyGraph(dependencyGraph)
    }

    def "population of a cyclic DependencyGraph with an includeRegexp"() {
        setup:
        // construct mock for gradle resolver results including the dependencies
        ResolvedModuleVersionResult rootMock = Mock(ResolvedModuleVersionResult) {
            getId() >> Mock(ModuleVersionIdentifier) {
                getGroup() >> 'group1'
                getName() >> 'name1'
                getVersion() >> '1.0.0'
            }
            getDependencies() >> [
                    Mock(ResolvedDependencyResult) {
                        getRequested() >> Mock(ModuleVersionSelector) {
                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                        }
                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                            getId() >> Mock(ModuleVersionIdentifier) {
                                getGroup() >> 'group2'
                                getName() >> 'name1'
                                getVersion() >> '1.0.0'
                            }
                            getDependencies() >> [
                                    Mock(ResolvedDependencyResult) {
                                        getRequested() >> Mock(ModuleVersionSelector) {
                                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                                        }
                                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                                            getId() >> Mock(ModuleVersionIdentifier) {
                                                getGroup() >> 'group1'
                                                getName() >> 'name1'
                                                getVersion() >> '9.9.9'
                                            }
                                            getDependencies() >> []
                                        }
                                    }
                            ]
                        }
                    },
                    Mock(ResolvedDependencyResult) {
                        getRequested() >> Mock(ModuleVersionSelector) {
                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                        }
                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                            getId() >> Mock(ModuleVersionIdentifier) {
                                getGroup() >> 'group2'
                                getName() >> 'name2'
                                getVersion() >> '1.0.0'
                            }
                            getDependencies() >> [
                                    Mock(ResolvedDependencyResult) {
                                        getRequested() >> Mock(ModuleVersionSelector) {
                                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                                        }
                                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                                            getId() >> Mock(ModuleVersionIdentifier) {
                                                getGroup() >> 'group1'
                                                getName() >> 'name1'
                                                getVersion() >> '1.0.0'
                                            }
                                            getDependencies() >> []
                                        }
                                    }
                            ]
                        }
                    }
            ]
        }

        // create mock for the gradle root project
        Project rootProject = createProjectMock(rootMock)

        // create empty DependencyGraph with a regexp
        DependencyGraph dependencyGraph = new DependencyGraph(rootProject: rootProject, includeRegexp: ".*1.0.0")

        when:
        dependencyGraph.populate()

        then:
        validateDependencyGraph(dependencyGraph)
    }

    def "population of a cyclic DependencyGraph with a resolved conflict"() {
        setup:
        // construct mock for gradle resolver results including the dependencies
        ResolvedModuleVersionResult rootMock = Mock(ResolvedModuleVersionResult) {
            getId() >> Mock(ModuleVersionIdentifier) {
                getGroup() >> 'group1'
                getName() >> 'name1'
                getVersion() >> '1.0.0'
            }
            getDependencies() >> [
                    Mock(ResolvedDependencyResult) {
                        getRequested() >> Mock(ModuleVersionSelector) {
                            matchesStrictly(_ as ModuleVersionIdentifier) >> false
                            getGroup() >> 'group2'
                            getName() >> 'name1'
                            getVersion() >> '1.0.0'
                        }
                        getSelected() >> Mock(ResolvedModuleVersionResult)
                    },
                    Mock(ResolvedDependencyResult) {
                        getRequested() >> Mock(ModuleVersionSelector) {
                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                        }
                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                            getId() >> Mock(ModuleVersionIdentifier) {
                                getGroup() >> 'group2'
                                getName() >> 'name2'
                                getVersion() >> '1.0.0'
                            }
                            getDependencies() >> [
                                    Mock(ResolvedDependencyResult) {
                                        getRequested() >> Mock(ModuleVersionSelector) {
                                            matchesStrictly(_ as ModuleVersionIdentifier) >> true
                                        }
                                        getSelected() >> Mock(ResolvedModuleVersionResult) {
                                            getId() >> Mock(ModuleVersionIdentifier) {
                                                getGroup() >> 'group1'
                                                getName() >> 'name1'
                                                getVersion() >> '1.0.0'
                                            }
                                            getDependencies() >> []
                                        }
                                    }
                            ]
                        }
                    }
            ]
        }

        // create mock for the gradle root project
        Project rootProject = createProjectMock(rootMock)

        // create empty DependencyGraph with a regexp
        DependencyGraph dependencyGraph = new DependencyGraph(rootProject: rootProject)

        when:
        dependencyGraph.populate()

        then:
        validateDependencyGraph(dependencyGraph)
    }

    private Project createProjectMock(ResolvedModuleVersionResult rootMock) {
        List<Configuration> configurationsMock = [Mock(Configuration) {
            getName() >> "config1"
            copyRecursive() >> Mock(Configuration) {
                getIncoming() >> Mock(ResolvableDependencies) {
                    getResolutionResult() >> Mock(ResolutionResult) {
                        getRoot() >> rootMock
                    }
                }
            }
        }]
        // the ConfigurationsContainer is a spy since an implementation of Set is needed for iteration over all elements
        ConfigurationContainer configurationContainerSpy =
            Spy(DefaultConfigurationContainer, constructorArgs: [null, null, null, null, null])
        configurationContainerSpy.addAll(configurationsMock)
        Project projectMock = Mock(Project) {
            getAllprojects() >> [
                    Mock(Project) {
                        getName() >> "project"
                        getConfigurations() >> configurationContainerSpy
                    }
            ]
        }
        return projectMock
    }

    private Boolean validateDependencyGraph(DependencyGraph dependencyGraph) {
        // validate nodes
        assert 3 == dependencyGraph.modules.size()
        assert 3 == dependencyGraph.moduleVersions.size()

        Node node1 = dependencyGraph.moduleVersions["group1:name1:1.0.0"]
        Node node2 = dependencyGraph.moduleVersions["group2:name1:1.0.0"]
        Node node3 = dependencyGraph.moduleVersions["group2:name2:1.0.0"]

        assert "group1:name1:1.0.0" == node1.toString()
        assert "group2:name1:1.0.0" == node2.toString()
        assert "group2:name2:1.0.0" == node3.toString()

        assert "group1:name1" == node1.moduleString
        assert "group2:name1" == node2.moduleString
        assert "group2:name2" == node3.moduleString

        assert [node1] == dependencyGraph.modules["group1:name1"]
        assert [node2] == dependencyGraph.modules["group2:name1"]
        assert [node3] == dependencyGraph.modules["group2:name2"]

        // validate edges
        assert 3 == dependencyGraph.incomingEdges.size()

        List<Edge> edges1 = dependencyGraph.incomingEdges["project#config1#group1:name1:1.0.0"]
        List<Edge> edges2 = dependencyGraph.incomingEdges["project#config1#group2:name1:1.0.0"]
        List<Edge> edges3 = dependencyGraph.incomingEdges["project#config1#group2:name2:1.0.0"]
        assert 1 == edges1.size()
        assert 1 == edges2.size()
        assert 1 == edges3.size()

        Edge edge1 = edges1.first()
        Edge edge2 = edges2.first()
        Edge edge3 = edges3.first()

        assert "project" == edge1.projectName
        assert "config1" == edge1.configurationName
        assert node3 == edge1.fromNode
        assert node1 == edge1.toNode

        assert "project" == edge2.projectName
        assert "config1" == edge2.configurationName
        assert node1 == edge2.fromNode
        assert node2 == edge2.toNode

        assert "project" == edge3.projectName
        assert "config1" == edge3.configurationName
        assert node1 == edge3.fromNode
        assert node3 == edge3.toNode

        // validate node edges
        assert [edge1] == node1.incoming
        assert ([edge2, edge3] as Set) == (node1.outgoing as Set)
        assert [edge2] == node2.incoming
        assert [] == node2.outgoing
        assert [edge3] == node3.incoming
        assert [edge1] == node3.outgoing

        return true
    }

}

package com.infinit.gradle.depcheck.internal

import spock.lang.Specification

class EdgeSpec extends Specification {

	def "get project configuration"() {
		setup:
		Edge edge = new Edge(projectName: 'project1', configurationName: 'configuration1')

		when:
		String projectConfiguration = edge.getProjectConfiguration()

		then:
		'project1#configuration1' == projectConfiguration
	}

	def "edge to string"() {
		setup:
		Node node1 = new Node(group: 'group1', name: 'name1', version: '1.0.0')
		Node node2 = new Node(group: 'group2', name: 'name2', version: '2.0.0')
		Edge edge = new Edge(projectName: 'prj', configurationName: 'conf', fromNode: node1, toNode: node2)

		when:
		String edgeString = edge.toString()

		then:
		'group1:name1:1.0.0->group2:name2:2.0.0' == edgeString
	}

	def "edge to detailed string"() {
		setup:
		Node node1 = new Node(group: 'group1', name: 'name1', version: '1.0.0')
		Node node2 = new Node(group: 'group2', name: 'name2', version: '2.0.0')
		Edge edge = new Edge(projectName: 'prj', configurationName: 'conf', fromNode: node1, toNode: node2)

		when:
		String edgeDetailedString = edge.toDetailedString()

		then:
		'prj:conf group1:name1:1.0.0->group2:name2:2.0.0' == edgeDetailedString
	}

	def "check for equality"() {
		when:
		Node node1 = new Node(group: 'group1', name: 'name1', version: '1.0.0')
		Node node2 = new Node(group: 'group1', name: 'name2', version: '1.0.0')
		Edge edge1 = new Edge(projectName: 'prj', configurationName: 'conf', fromNode: node1, toNode: node2)
		Edge edge2 = new Edge(projectName: 'prj', configurationName: 'conf', fromNode: node1, toNode: node2)

		then:
		edge1 == edge2
		edge1.equals(edge2)
	}

}

package com.infinit.gradle.depcheck.internal

import spock.lang.Specification

class NodeSpec extends Specification {

	def "node to string"() {
		setup:
		Node node = new Node(group: 'group1', name: 'name1', version: '1.0.0')

		when:
		String nodeString = node.toString()

		then:
		'group1:name1:1.0.0' == nodeString
	}

	def "get module string"() {
		setup:
		Node node = new Node(group: 'group1', name: 'name1', version: '1.0.0')

		when:
		String moduleString = node.getModuleString()

		then:
		'group1:name1' == moduleString
	}

	def "check for equality"() {
		when:
		Node node1 = new Node(group: 'group1', name: 'name1', version: '1.0.0')
		Node node2 = new Node(group: 'group1', name: 'name1', version: '1.0.0')

		then:
		node1 == node2
		node1.equals(node2)
	}

}

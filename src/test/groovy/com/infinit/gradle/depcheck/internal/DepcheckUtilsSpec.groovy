package com.infinit.gradle.depcheck.internal

import com.infinit.gradle.depcheck.DepcheckPlugin
import com.infinit.gradle.depcheck.DepcheckPluginExtension
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class DepcheckUtilsSpec extends Specification {

	def "create dependency graph"() {
		setup:
		Project project = ProjectBuilder.builder().build()
		project.extensions.create(DepcheckPlugin.DEPCHECK_PLUGIN_EXTENSION, DepcheckPluginExtension)

		when:
		DependencyGraph dependencyGraph = DepcheckUtils.getOrCreateDependencyGraph(project)

		then:
		dependencyGraph
	}

	def "get dependency graph"() {
		setup:
		DependencyGraph mockedDependencyGraph = new DependencyGraph()
		Project project = ProjectBuilder.builder().build()
		DepcheckPluginExtension depcheckPluginExtension = project.extensions.create(DepcheckPlugin.DEPCHECK_PLUGIN_EXTENSION, DepcheckPluginExtension)
		depcheckPluginExtension.dependencyGraph = mockedDependencyGraph

		when:
		DependencyGraph dependencyGraph = DepcheckUtils.getOrCreateDependencyGraph(project)

		then:
		mockedDependencyGraph == dependencyGraph
	}
}

package com.infinit.gradle.depcheck

import ch.qos.logback.classic.Level
import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.testutils.DependencyGraphTestUtils
import com.infinit.gradle.depcheck.testutils.LoggerUtils
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

@Mixin([DependencyGraphTestUtils, LoggerUtils])
class DepcheckCyclesTaskSpec extends Specification {

	def setup() {
		attachTestAppender(Level.WARN)
	}

	def cleanup() {
		detachTestAppender()
	}

	def "can add task to project"() {
		setup:
		Project project = ProjectBuilder.builder().build()

		when:
		Task task = project.task('depcheckCycles', type: DepcheckCyclesTask)

		then:
		task instanceof DepcheckCyclesTask
	}

	def "no false positive for an acyclic graph"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				node("g:3:0")
				edge("g:1:0", "g:2:0")
				edge("g:1:0", "g:3:0")
			}
		}
		DepcheckCyclesTask task = setupTaskWithDependencyGraph(graphDefinition)

        when:
		task.depcheckCycles()

		then:
        logAsString == ""
	}

	def "error on single cycle"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				node("g:3:0")
				node("g:4:0")
				edge("g:1:0", "g:2:0")
				edge("g:2:0", "g:3:0")
				edge("g:2:0", "g:4:0")
				edge("g:4:0", "g:2:0") // create cycle
			}
		}
		DepcheckCyclesTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckCycles()

		then:
        thrown(GradleException)
        logAsString == "[ERROR cycle in dependency graph found:][ERROR g:2:0->g:4:0->g:2:0]"
	}

	def "error on two cycles"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				node("g:3:0")
				node("g:4:0")
				edge("g:1:0", "g:2:0")
				edge("g:2:0", "g:3:0")
				edge("g:2:0", "g:4:0")
				edge("g:4:0", "g:2:0") // create cycle 1
				edge("g:3:0", "g:1:0") // create cycle 2
			}
		}
		DepcheckCyclesTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckCycles()

		then:
        thrown(GradleException)
		logAsString == "[ERROR cycle in dependency graph found:][ERROR g:1:0->g:2:0->g:3:0->g:1:0][ERROR cycle in dependency graph found:][ERROR g:2:0->g:4:0->g:2:0]"
	}

	def "report only one path through the cycle"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				node("g:3:0")
				node("g:4:0")
				edge("g:1:0", "g:2:0")
				edge("g:1:0", "g:3:0")
				edge("g:2:0", "g:4:0")
				edge("g:3:0", "g:4:0") // join the paths
				edge("g:4:0", "g:1:0") // create cycle
			}
		}
		DepcheckCyclesTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckCycles()

		then:
        thrown(GradleException)
        logAsString == "[ERROR cycle in dependency graph found:][ERROR g:1:0->g:2:0->g:4:0->g:1:0]"
	}

	private DepcheckCyclesTask setupTaskWithDependencyGraph(def graphDefinition) {
		DependencyGraph dependencyGraph = createDependencyGraph(graphDefinition)

		// mock static methods in DepcheckUtils:
		GroovyMock(DepcheckUtils, global: true)
		DepcheckUtils.getOrCreateDependencyGraph(_) >> dependencyGraph

		// create DepcheckCyclesTask
		return (DepcheckCyclesTask) ProjectBuilder.builder().build().task('depcheckCycles', type: DepcheckCyclesTask)
	}

}

package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.testutils.DependencyGraphTestUtils
import groovy.io.GroovyPrintWriter
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

@Mixin(DependencyGraphTestUtils)
class CreateDependencyDotTaskSpec extends Specification {

	def "can add task to project"() {
		setup:
		Project project = ProjectBuilder.builder().build()

		when:
		Task task = project.task('createDependencyDot', type: CreateDependencyDotTask)

		then:
		task instanceof CreateDependencyDotTask
	}

	def "generate dot result of graph"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project", "config1") {
				node("group1:name1:1.0.0")
				node("group2:name1:1.0.0")
				node("group2:name1:1.0.1")
				edge("group1:name1:1.0.0", "group2:name1:1.0.0")
				edge("group1:name1:1.0.0", "group2:name1:1.0.1")
			}
		}
		DependencyGraph dependencyGraph = createDependencyGraph(graphDefinition)

		// mock static methods in DepcheckUtils:
		GroovyMock(DepcheckUtils, global: true)
		DepcheckUtils.getOrCreateDependencyGraph(_) >> dependencyGraph

		// create CreateDependencyDotTask and configure it to write to ByteArrayOutputStream
		CreateDependencyDotTask task = (CreateDependencyDotTask) ProjectBuilder.builder().build().task('createDependencyDot', type: CreateDependencyDotTask)
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()
		task.createPrintWriter = { -> (new GroovyPrintWriter(byteArrayOutputStream)) as PrintWriter }
        String nl = System.getProperty("line.separator")
        String expectedResult = "digraph Compile {$nl" +
		        "\tgraph [rankdir=LR, fontsize=10, margin=0.001];$nl" +
		        "\tnode [shape=box, color=black];$nl" +
		        "\t\"group1:name1:1.0.0\";$nl" +
		        "\tsubgraph \"cluster_group2:name1\" {$nl" +
		        "\tstyle=filled;$nl" +
		        "\tcolor=lightgrey;$nl" +
		        "\t\t\"group2:name1:1.0.0\";$nl" +
		        "\t\t\"group2:name1:1.0.1\";$nl" +
		        "\t}$nl" +
		        "\t\"group1:name1:1.0.0\" -> \"group2:name1:1.0.0\";$nl" +
		        "\t\"group1:name1:1.0.0\" -> \"group2:name1:1.0.1\";$nl" +
		        "}$nl"

        when:
		task.createDependencyDot()

		then:
		String text = byteArrayOutputStream.toString()
         text == expectedResult
	}

}

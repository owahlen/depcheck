package com.infinit.gradle.depcheck

import ch.qos.logback.classic.Level
import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.testutils.DependencyGraphTestUtils
import com.infinit.gradle.depcheck.testutils.LoggerUtils
import groovy.io.GroovyPrintStream
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

@Mixin([DependencyGraphTestUtils, LoggerUtils])
class DepcheckRedundantTaskSpec extends Specification {

	def setup() {
		attachTestAppender(Level.WARN)
	}

	def cleanup() {
		detachTestAppender()
	}

	def "can add task to project"() {
		setup:
		Project project = ProjectBuilder.builder().build()

		when:
		Task task = project.task('depcheckRedundant', type: DepcheckRedundantTask)

		then:
		task instanceof DepcheckRedundantTask
	}

	def "no warning on a graph that even has a cycle"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				node("g:3:0")
				node("g:4:0")
				edge("g:1:0", "g:2:0")
				edge("g:2:0", "g:3:0")
				edge("g:2:0", "g:4:0")
				edge("g:4:0", "g:2:0") // create cycle
			}
		}
		DepcheckRedundantTask task = setupTaskWithDependencyGraph(graphDefinition)

        when:
		task.depcheckRedundant()

		then:
        logAsString == ""
	}

	def "no warning on a graph where the paths join after a pathlenght greater than one"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				node("g:3:0")
				node("g:4:0")
				edge("g:1:0", "g:2:0")
				edge("g:1:0", "g:3:0")
				edge("g:2:0", "g:4:0")
				edge("g:3:0", "g:4:0") // join point of diamond
			}
		}
		DepcheckRedundantTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckRedundant()

		then:
        logAsString == ""
	}

	def "warning on a redundant edge where the redundancy is not caused by a sibling edge sharing the same fromNode and toNode"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				node("g:3:0")
				edge("g:1:0", "g:3:0") // this edge is redundant
				edge("g:1:0", "g:2:0")
				edge("g:2:0", "g:3:0")
			}
		}
		DepcheckRedundantTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckRedundant()

		then:
        logAsString == "[WARN redundant edge found: 'project1:config1 g:1:0->g:3:0']"
	}

	def "warning on a redundant edge where the redundancy is caused by two sibling edge sharing the same fromNode and toNode"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				edge("g:1:0", "g:2:0") // this edge is redundant
				edge("g:1:0", "g:2:0")
				edge("g:1:0", "g:2:0")
			}
		}
		DepcheckRedundantTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckRedundant()

		then:
        logAsString == "[WARN redundant edge found: 'project1:config1 g:1:0->g:2:0']"
	}

	def "no warning on other edges sharing the same fromNode and toNode but are located in another project"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				edge("g:1:0", "g:2:0")
			}
			projectConfiguration("project2", "config1") {
				node("g:1:0")
				node("g:2:0")
				edge("g:1:0", "g:2:0")
			}
		}
		DepcheckRedundantTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckRedundant()

		then:
        logAsString == ""
	}

	def "no warning on other edges sharing the same fromNode and toNode but are located in another configuration"() {
		setup:
		// setup dependency graph
		def graphDefinition = {
			projectConfiguration("project1", "config1") {
				node("g:1:0")
				node("g:2:0")
				edge("g:1:0", "g:2:0")
			}
			projectConfiguration("project1", "config2") {
				node("g:1:0")
				node("g:2:0")
				edge("g:1:0", "g:2:0")
			}
		}
		DepcheckRedundantTask task = setupTaskWithDependencyGraph(graphDefinition)

		when:
		task.depcheckRedundant()

		then:
        logAsString == ""
	}

	private DepcheckRedundantTask setupTaskWithDependencyGraph(def graphDefinition) {
		DependencyGraph dependencyGraph = createDependencyGraph(graphDefinition)

		// mock static methods in DepcheckUtils:
		GroovyMock(DepcheckUtils, global: true)
		DepcheckUtils.getOrCreateDependencyGraph(_) >> dependencyGraph

		// create DepcheckRedundantTask
		return (DepcheckRedundantTask) ProjectBuilder.builder().build().task('depcheckRedundant', type: DepcheckRedundantTask)
	}

}

package com.infinit.gradle.depcheck

import groovy.io.GroovyPrintWriter
import org.custommonkey.xmlunit.Diff
import org.custommonkey.xmlunit.XMLUnit
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.ModuleVersionIdentifier
import org.gradle.api.artifacts.ResolvableDependencies
import org.gradle.api.artifacts.result.ResolutionResult
import org.gradle.api.artifacts.result.ResolvedModuleVersionResult
import org.gradle.api.internal.artifacts.configurations.DefaultConfigurationContainer
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class CreateDependencyXmlTaskSpec extends Specification {

    def "can add task to project"() {
        setup:
        Project project = ProjectBuilder.builder().build()

        when:
        Task task = project.task('createDependencyXml', type: CreateDependencyXmlTask)

        then:
        task instanceof CreateDependencyXmlTask
    }

    def "generate xml result of graph"() {
        setup:
        Project project = createProjectMock()

        // create CreateDependencyXmlTask and configure it to write to ByteArrayOutputStream
        CreateDependencyXmlTask task = (CreateDependencyXmlTask) ProjectBuilder.builder().build().task('createDependencyXml', type: CreateDependencyXmlTask)
        task.setProject(project)
        StringWriter stringWriter = new StringWriter()
        task.createWriterForDependencyXml = {-> stringWriter as Writer }
        String expectedResult = """
            <dependencies>
                <project path=':project'>
                    <configuration name='config1'>
                        <dependency group='group1' name='name1' version='1.0.0' />
                        <dependency group='group1' name='name2' version='1.0.0' />
                    </configuration>
                </project>
            </dependencies>
        """
        XMLUnit.setIgnoreWhitespace(true)

        when:
        task.createDependencyXml()

        then:
        Diff xmlDiff = new Diff(stringWriter.toString(), expectedResult)
        xmlDiff.similar()
    }

    private ProjectInternal createProjectMock() {
        List<Configuration> configurationsMock = [Mock(Configuration) {
            getName() >> "config1"
            copyRecursive() >> Mock(Configuration) {
                getIncoming() >> Mock(ResolvableDependencies) {
                    getResolutionResult() >> Mock(ResolutionResult) {
                        getAllModuleVersions() >> [
                                Mock(ResolvedModuleVersionResult) {
                                    toString() >> 'group1:name1:1.0.0'
                                    getId() >> Mock(ModuleVersionIdentifier) {
                                        getGroup() >> 'group1'
                                        getName() >> 'name1'
                                        getVersion() >> '1.0.0'
                                    }
                                },
                                Mock(ResolvedModuleVersionResult) {
                                    toString() >> 'group1:name2:1.0.0'
                                    getId() >> Mock(ModuleVersionIdentifier) {
                                        getGroup() >> 'group1'
                                        getName() >> 'name2'
                                        getVersion() >> '1.0.0'
                                    }
                                }
                        ]
                    }
                }
            }
        }]
        // the ConfigurationsContainer is a spy since an implementation of Set is needed for iteration over all elements
        ConfigurationContainer configurationContainerSpy =
            Spy(DefaultConfigurationContainer, constructorArgs: [null, null, null, null, null])
        configurationContainerSpy.addAll(configurationsMock)
        ProjectInternal projectMock = Mock(ProjectInternal) {
            getRootProject() >> Mock(ProjectInternal) {
                allprojects(_ as Closure) >> { Closure allprojectsClosure ->
                    [
                            Mock(ProjectInternal) {
                                getPath() >> ":project"
                                getConfigurations() >> configurationContainerSpy
                            }
                    ].each(allprojectsClosure)
                }
            }
        }
        return projectMock
    }

}

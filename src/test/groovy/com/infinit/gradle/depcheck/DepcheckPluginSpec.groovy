package com.infinit.gradle.depcheck

import org.gradle.api.Task
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import spock.lang.Specification

class DepcheckPluginSpec extends Specification {

	def "depcheck plugin adds relevant tasks to project"() {

		setup:
		Project project = ProjectBuilder.builder().build()

		when:
		project.apply plugin: 'depcheck'

		then:
		project.tasks.getByName('depcheck')
		project.tasks.getByName('depcheckConflicts') instanceof DepcheckConflictsTask
		project.tasks.getByName('depcheckRedundant') instanceof DepcheckRedundantTask
		project.tasks.getByName('createDependencyDot') instanceof CreateDependencyDotTask

	}

	def "depcheck task depends on other check tasks"() {

		setup:
		Project project = ProjectBuilder.builder().build()

		when:
		project.apply plugin: 'depcheck'

		then:
		Task depcheckTask = project.tasks.getByName('depcheck')
		Set<Task> dependentTasks = depcheckTask.taskDependencies.getDependencies(depcheckTask)
		dependentTasks.find { it instanceof DepcheckConflictsTask }
		dependentTasks.find { it instanceof DepcheckRedundantTask }
        dependentTasks.find { it instanceof DepcheckCyclesTask }
	}
}

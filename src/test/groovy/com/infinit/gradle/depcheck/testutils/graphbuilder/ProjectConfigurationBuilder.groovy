package com.infinit.gradle.depcheck.testutils.graphbuilder

import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.internal.Edge
import com.infinit.gradle.depcheck.internal.Node

/**
 * Class to build a project configuration in the context of a DependencyGraphBuilder
 *
 * User: owahlen
 * Date: 30.07.13
 */
class ProjectConfigurationBuilder {

	private DependencyGraph dependencyGraph
	private String project
	private String configuration

	void node(String moduleVersion) {
		Node node = createNode(moduleVersion)
		assert node == dependencyGraph.addOrLookupNode(node) : "The node $node already exists in the graph"
	}

	void edge(String fromModuleVersion, String toModuleVersion) {
		Node fromNode = dependencyGraph.moduleVersions[fromModuleVersion]
		assert fromNode: "The node $fromModuleVersion does not exist in the graph"
		Node toNode = dependencyGraph.moduleVersions[toModuleVersion]
		assert toNode: "The node $toModuleVersion does not exist in the graph"
		Edge edge = new Edge(fromNode: fromNode, toNode: toNode, projectName: project, configurationName: configuration)
		dependencyGraph.addEdge(edge)
	}

	private Node createNode(String moduleVersion) {
		def (group,name,version) = moduleVersion.split(':')
		assert group
		assert name
		assert version
		return new Node(group:group, name:name, version:version)
	}
}

package com.infinit.gradle.depcheck.testutils

import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.testutils.graphbuilder.DependencyGraphBuilder

/**
 * Helper class that contains graph operation used in tests.
 * The class can be mixed into tests.
 *
 * User: owahlen
 * Date: 30.07.13
 */
class DependencyGraphTestUtils {

    DependencyGraph createDependencyGraph(Closure graphDefinition) {
	    DependencyGraphBuilder dependencyGraphBuilder = new DependencyGraphBuilder()
	    graphDefinition.delegate = dependencyGraphBuilder
	    graphDefinition.call()
	    return dependencyGraphBuilder.dependencyGraph
    }

}

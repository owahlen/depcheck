package com.infinit.gradle.depcheck.testutils

import ch.qos.logback.classic.Level
import com.infinit.gradle.depcheck.testutils.logging.ConfigureLogging
import com.infinit.gradle.depcheck.testutils.logging.TestAppender

/**
 * Helper class that adds an appender to the root logger.
 * This appender is able to capture log output which is useful for tests.
 *
 * User: owahlen
 * Date: 09.08.13
 */
class LoggerUtils {
	TestAppender testAppender
	ConfigureLogging logging

	/**
	 * Attach the testAppender
	 */
	void attachTestAppender(Level level = Level.ALL) {
		assert !testAppender: 'testAppender is already attached'
		testAppender = new TestAppender()
		logging = new ConfigureLogging(testAppender)
		logging.attachAppender()
		logging.setLevel(level)
	}

	/**
	 * Detach the testAppender
	 */
	void detachTestAppender() {
		assert testAppender: 'testAppender is not attached'
		logging.detachAppender()
		testAppender = null
		logging = null
	}

	/**
	 * get the captured log content as String
	 * @return log content as String
	 */
	String getLogAsString() {
		assert testAppender: 'testAppender is not attached'
		return testAppender.toString()
	}

}

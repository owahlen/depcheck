package com.infinit.gradle.depcheck.testutils.graphbuilder

import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.internal.Node

/**
 * Builder for a DependencyGraph
 * This class acts as the delegate for graphDefinitions
 *
 * User: owahlen
 * Date: 28.07.13
 */
class DependencyGraphBuilder {

	// DependencyGraph to be populated
	private DependencyGraph dependencyGraph = new DependencyGraph()

	// DependencyGraph is read only
	DependencyGraph getDependencyGraph() {
		return dependencyGraph
	}

	void projectConfiguration(String project, String configuration, Closure closure) {
		ProjectConfigurationBuilder builder = new ProjectConfigurationBuilder(
				dependencyGraph: dependencyGraph, project:project, configuration:configuration
		)
		closure.delegate = builder
		closure.call()
	}

}

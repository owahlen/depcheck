package com.infinit.gradle.depcheck

import groovy.xml.MarkupBuilder
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.result.ResolvedModuleVersionResult

/**
 * Flatten the DependencyGraph and dump it into an xml file.
 *
 * User: owahlen
 * Date: 14.10.13
 */
abstract class AbstractDependencyXmlTask extends DefaultTask {

    final static public String DEFAULT_DEPENDENCY_XML = 'dependency.xml'
    final static public String DEFAULT_DEPENDENCY_DIFF = 'dependencyDiff.csv'

    @SuppressWarnings("GroovyAssignabilityCheck")
    protected void dumpDependencyGraph(MarkupBuilder builder) {
        builder.'dependencies' {
            project.rootProject.allprojects { Project prj ->
                logger.lifecycle("analyzing project ${prj.path}...")
                'project'('path': prj.path) {
                    getFilteredProjectConfigurations(prj).each { Configuration config ->
                        'configuration'('name': config.name) {
                            // todo: Gradle 1.6 stores resolved configurations in a way that can cause heap space exceptions on big projects.
                            // As a workaround the configuration is duplicated, the duplicate is resolved and then released from memory.
                            // This reduces performance and should be removed when the problem is fixed in Gradle 1.7
                            Configuration configCopy = config.copyRecursive()
                            Set<ResolvedModuleVersionResult> moduleVersions = configCopy.incoming.resolutionResult.allModuleVersions
                            moduleVersions.sort { it.toString() }.each { ResolvedModuleVersionResult moduleVersion ->
                                'dependency'(
                                        'group': moduleVersion.id.group,
                                        'name': moduleVersion.id.name,
                                        'version': moduleVersion.id.version
                                )
                            }
                        }
                    }
                }
            }
        }
    }

    protected Set<Configuration> getFilteredProjectConfigurations(Project project) {
        return project.configurations
    }

}

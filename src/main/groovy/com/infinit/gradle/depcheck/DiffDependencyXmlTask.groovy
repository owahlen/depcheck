package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.ModuleDependency
import groovy.io.GroovyPrintWriter
import groovy.util.slurpersupport.GPathResult
import groovy.xml.MarkupBuilder
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.TaskAction

/**
 * Flatten the DependencyGraph and dump it into an xml file.
 *
 * User: owahlen
 * Date: 14.10.13
 */
class DiffDependencyXmlTask extends AbstractDependencyXmlTask {

    DepcheckPluginExtension extension

    DiffDependencyXmlTask() {
        extension = DepcheckUtils.getExtension(project)
    }

    @TaskAction
    def diffDependencyXml() {
        GPathResult previousDependencyXml = readDependencyXml()
        GPathResult currentDependencyXml = createDependencyXml()
        PrintWriter writer = createPrintWriterForDependencyDiff()
        writeDependencyXmlDifferences(previousDependencyXml, currentDependencyXml, writer)
        writer.close()
    }

    Closure<GPathResult> readDependencyXml = {->
        String dependencyXml = extension.dependencyXml ?: DEFAULT_DEPENDENCY_XML
        File dependencyXmlFile = project.rootProject.file(dependencyXml)
        assert dependencyXmlFile.exists(): "unable to find the file '${dependencyXml}' in the root project!"
        return new XmlSlurper().parse(dependencyXmlFile)
    }

    Closure<PrintWriter> createPrintWriterForDependencyDiff = {->
        String dependencyDiff = extension.dependencyDiff ?: DEFAULT_DEPENDENCY_DIFF
        logger.lifecycle("writing dependency diff of project to file ${dependencyDiff}...")
        File dependencyDiffFile = project.rootProject.file(dependencyDiff)
        // since the CSV is supposed to be read by Excel it is encoded in Windows Latin-1 (Cp1252)
        return (new GroovyPrintWriter(dependencyDiffFile, 'windows-1252') as PrintWriter)
    }

    protected Set<Configuration> getFilteredProjectConfigurations(Project project) {
        String configurationDiffRegexp = extension.configurationDiffRegexp
        if (configurationDiffRegexp) {
            return project.configurations.findAll { it.name =~ configurationDiffRegexp }
        } else {
            return project.configurations
        }
    }

    private GPathResult createDependencyXml() {
        Writer writer = new StringWriter()
        MarkupBuilder builder = new MarkupBuilder(writer)
        dumpDependencyGraph(builder)
        writer.close()
        return new XmlSlurper().parseText(writer.toString())
    }

    private void writeDependencyXmlDifferences(GPathResult previousDependencyXml,
                                               GPathResult currentDependencyXml, PrintWriter writer) {
        Boolean hideModuleInDiff = extension.hideModuleInDiff

        writer.println 'sep=;'
        if (hideModuleInDiff) {
            writer.println(['project', 'group', 'previous_version', 'current_version'].join(';'))
        } else {
            writer.println(['project', 'group', 'module', 'previous_version', 'current_version'].join(';'))
        }
        Map<String, ModuleDependency> moduleDependencies = [:]
        populateModuleDependencies(moduleDependencies, previousDependencyXml) { it.&setPreviousVersion }
        populateModuleDependencies(moduleDependencies, currentDependencyXml) { it.&setCurrentVersion }
        List<ModuleDependency> filteredModuleDependencies = filterModuleDependencies(moduleDependencies.values())
        filteredModuleDependencies.each { ModuleDependency dep ->
            if (hideModuleInDiff) {
                writer.println([dep.projectName, dep.group,
                        '"=""' + (dep.previousVersion ?: '') + '"""', '"=""' + (dep.currentVersion ?: '') + '"""'].join(';'))
            } else {
                writer.println([dep.projectName, dep.group, dep.module,
                        '"=""' + (dep.previousVersion ?: '') + '"""', '"=""' + (dep.currentVersion ?: '') + '"""'].join(';'))
            }
        }
    }

    private void populateModuleDependencies(Map<String, ModuleDependency> moduleDependencies,
                                            GPathResult dependencyXml, Closure getVersionSetter) {
        List<GPathResult> dependencies = dependencyXml.depthFirst().findAll { it.name() == 'dependency' }
        dependencies.each { GPathResult dependency ->
            ModuleDependency tempModuleDependency = new ModuleDependency(
                    projectName: dependency.parent().parent().@path,
                    configurationName: dependency.parent().@name,
                    group: dependency.@group,
                    module: dependency.@name
            )
            ModuleDependency moduleDependency = moduleDependencies[tempModuleDependency.projectModule]
            if (!moduleDependency) {
                moduleDependency = tempModuleDependency
                moduleDependencies[tempModuleDependency.projectModule] = tempModuleDependency
            }
            String version = dependency.@version
            Closure setVersion = getVersionSetter(moduleDependency)
            setVersion(version)
        }
    }

    private List<ModuleDependency> filterModuleDependencies(Collection<ModuleDependency> moduleDependencies) {
        Boolean hideModuleInDiff = extension.hideModuleInDiff

        // find moduleDependencies with a difference in module version
        List<ModuleDependency> versionDeltas = moduleDependencies.findAll { ModuleDependency moduleDependency ->
            moduleDependency.previousVersion != moduleDependency.currentVersion
        }

        // filter on the versionDeltas that contain configurations matching the configurationDiffRegexp
        if(extension.configurationDiffRegexp) {
            versionDeltas = versionDeltas.findAll { it.configurationName =~ extension.configurationDiffRegexp }
        }

        // eliminate the configuration column
        versionDeltas.each { it.configurationName = '' }
        if(hideModuleInDiff) {
            // eliminate the module column
            versionDeltas.each { it.module = '' }
        }
        // only leave records that differ then the list of versionDeltas and sort them
        versionDeltas.unique()
        versionDeltas.sort { it.projectModule }
    }

}

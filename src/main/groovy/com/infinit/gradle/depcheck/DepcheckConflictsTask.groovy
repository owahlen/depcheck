package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.internal.Edge
import com.infinit.gradle.depcheck.internal.Node
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.TaskAction

import java.util.regex.Matcher

/**
 * This gradle task checks if the dependency graph of the project contains artifacts
 * that share the same group and name but have different versions.
 * This is considered a version conflict and a warning is issued.
 *
 * User: owahlen
 * Date: 28.07.13
 */
class DepcheckConflictsTask extends DefaultTask {

	@TaskAction
	def depcheckConflicts() {
		DependencyGraph dependencyGraph = DepcheckUtils.getOrCreateDependencyGraph(project)
		checkForConflictingArtifactVersions(dependencyGraph)
	}

	/**
     * Using the conflictingVersionRegexp it is defined which part of the version identifies breaking changes.
     * This method identifies all artifacts that share the same group and name
     * but have different conflicting version substrings.
	 * @param dependencyGraph of artifacts
	 */
	private void checkForConflictingArtifactVersions(DependencyGraph dependencyGraph) {
		// dependencyGraph.modules contains collections of artifacts that share group and name.
		// If any of these collections has more than one element it represents a conflict.
		List<List<Node>> conflicts = findConflictingModuleValues(dependencyGraph.modules.values())
		conflicts.each { List<Node> conflict ->
			logger.error("Version conflict found for module '${conflict.first().moduleString}':")
			conflict.sort {"$it"}.each { Node node ->
				logger.error("  '$node' referenced from:")
				Map<String, Boolean> conflictingLines = [:]
				node.incoming.each { Edge incomingEdge ->
					String conflictingLine = "    '$incomingEdge.fromNode'"
					if (!conflictingLines.containsKey(conflictingLine)) {
						logger.error(conflictingLine)
						conflictingLines[conflictingLine] = true
					}
				}
			}
		}
        if(conflicts) {
            throw new GradleException('Version conflicts found')
        }
	}

    /**
     * A module is a List of nodes that share the same group and name.
     * This method extracts those lists that contain at least a pair of nodes with conflicting versions.
     * @param moduleValues Collection of List of Nodes that share the same group and name
     * @return Collection of List of Nodes that contain version conflicts
     */
    Collection<List<Node>> findConflictingModuleValues(Collection<List<Node>> moduleValues) {
        return moduleValues.findAll { List<Node> nodes ->
            if(nodes.size() <= 1) {
                // the module <group>:<name> is referenced only once so the version cannot conflict
                return false
            }
            // try to find two nodes whose relevant version part differs
            List<String> conflictingVersionSubstrings = nodes.collect { getConflictingVersionSubstring(it.version) }.unique()
            return conflictingVersionSubstrings.size()>1
        }
    }

    /**
     * Given a concrete version this method returns the part of the version that identifies a breaking change.
     * It uses the conflictingVersionRegexp in order to identify the substring.
     * If this variable is not set or does not match the version string the entire version string is returned
     * @param version whose conflicting version substring is to be extracted
     * @return conflicting version substring
     */
    String getConflictingVersionSubstring(String version) {
        String conflictingVersionRegexp = DepcheckUtils.getExtension(project).conflictingVersionRegexp

        if(!conflictingVersionRegexp) {
            // The entire version string is considered for identifying a breaking change
            return version
        }
        Matcher matcher = version =~ conflictingVersionRegexp
        if(!matcher.find()) {
            // The version does not match the pattern.
            // Therefore the whole version string is considered relevant for identifying a breaking change.
            return version
        }
        List<String> groups = (List<String>) matcher[0]
        return groups[1]
    }
}
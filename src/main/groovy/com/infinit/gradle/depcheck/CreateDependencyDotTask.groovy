package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.internal.Edge
import com.infinit.gradle.depcheck.internal.Node
import groovy.io.GroovyPrintWriter
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * Dump the DependencyGraph into a dot file.
 * This file format is specified within the graphviz project.
 * @see <a href="http://www.graphviz.org">http://www.graphviz.org</a>
 *
 * User: owahlen
 * Date: 28.07.13
 */
class CreateDependencyDotTask extends DefaultTask {

	@TaskAction
	def createDependencyDot() {
		DependencyGraph dependencyGraph = DepcheckUtils.getOrCreateDependencyGraph(project)
		PrintWriter writer = createPrintWriter()
		dumpDependencyGraph(dependencyGraph, writer)
		writer.close()
	}

	Closure<PrintWriter> createPrintWriter = {->
		// target directory and file
		File reportsDir = project.file('build/reports')
		reportsDir.mkdirs()
		File depsDotFile = new File(reportsDir, 'deps.dot')
		return (new GroovyPrintWriter(depsDotFile) as PrintWriter)
	}

	private void dumpDependencyGraph(DependencyGraph dependencyGraph, PrintWriter writer) {
		writer.println 'digraph Compile {'
		writer.println '\tgraph [rankdir=LR, fontsize=10, margin=0.001];'
		writer.println '\tnode [shape=box, color=black];'

		dependencyGraph.modules.each { String module, List<Node> nodes ->
			if (nodes.size() > 1) {
				// cluster several versions of an artifact together
				writer.println "\tsubgraph \"cluster_$module\" {"
				writer.println '\tstyle=filled;'
				writer.println '\tcolor=lightgrey;'
				for (Node node : nodes) {
					writer.println "\t\t\"$node\";"
				}
				writer.println "\t}"
			} else {
				for (Node node : nodes) {
					writer.println "\t\"$node\";"
				}
			}
		}

		Map<String, Boolean> dependencyLines = [:]
		dependencyGraph.incomingEdges.values().flatten().each { Edge edge ->
			String dependencyLine = "\"$edge.fromNode\" -> \"$edge.toNode\""
			//String dependencyLine = "\"$edge.fromNode\" -> \"$edge.toNode\" [label=\"$edge.projectConfiguration\"]"
			if (!dependencyLines.containsKey(dependencyLine)) {
				writer.println "\t$dependencyLine;"
				dependencyLines[dependencyLine] = true
			}

		}
		writer.println "}"
	}

}

package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DepcheckUtils
import groovy.io.GroovyPrintWriter
import groovy.xml.MarkupBuilder
import org.gradle.api.tasks.TaskAction

/**
 * Flatten the DependencyGraph and dump it into an xml file.
 *
 * User: owahlen
 * Date: 14.10.13
 */
class CreateDependencyXmlTask extends AbstractDependencyXmlTask {

    @TaskAction
    def createDependencyXml() {
        Writer writer = createWriterForDependencyXml()
        MarkupBuilder builder = new MarkupBuilder(writer)
        dumpDependencyGraph(builder)
        writer.close()
    }

    Closure<Writer> createWriterForDependencyXml = {->
        String dependencyXml = DepcheckUtils.getExtension(project).dependencyXml ?: DEFAULT_DEPENDENCY_XML
        logger.lifecycle("writing dependencies of project to file ${dependencyXml}...")
        File dependencyXmlFile = project.rootProject.file(dependencyXml)
        return ((new GroovyPrintWriter(dependencyXmlFile)) as Writer)
    }

}

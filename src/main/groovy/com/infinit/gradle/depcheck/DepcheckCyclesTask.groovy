package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.internal.Node
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.TaskAction

/**
 * This gradle task checks if the dependency graph contains cycles.
 * A cycle in the dependency graph means that there is a chain of dependency from an artifact back to the artifact itself.
 * A warning is issued for such a cyclic dependency
 *
 * User: owahlen
 * Date: 01.08.13
 */
class DepcheckCyclesTask extends DefaultTask {

	@TaskAction
	def depcheckCycles() {
		DependencyGraph dependencyGraph = DepcheckUtils.getOrCreateDependencyGraph(project)
		checkForCycles(dependencyGraph)
	}

	/**
	 * Check the dependency graph for cycles and print each cycle
	 * @param dependencyGraph of artifacts
	 */
	private void checkForCycles(DependencyGraph dependencyGraph) {
		List<List<Node>> cycles = findCycles(dependencyGraph)
		for(List<Node> cycle : cycles) {
            logger.error('cycle in dependency graph found:')
			logger.error(cycle*.toString().join("->"))
		}
        if(cycles) {
            throw new GradleException('cycles found in dependency graph')
        }
	}

	/**
	 * Iterate through all nodes of the dependency graph and check if the current node is the source of a cycle.
	 * The function returns a list of cycles found.
	 * Each cycle is in turn represented by a list of nodes that participate in a cyclic dependency chain.
	 * @param dependencyGraph of artifacts
	 * @return list of cycles found
	 */
	private List<List<Node>> findCycles(DependencyGraph dependencyGraph) {
		List<List<Node>> cycles = [] // list of dependency chains containing cycles
		Map<Node, Boolean> visited = [:] // nodes that have already been analyzed
		for(Node node : dependencyGraph.moduleVersions.values()) {
			if(visited.containsKey(node)) {
				 continue // go on to the next node
			}
			List<Node> nodeStack = [] // node chain of a depth first graph traversal
			depthFirstTraversal(node, nodeStack, visited, cycles)
		}
		return cycles
	}

	/**
	 * Perform a depth first traversal of the dependency graph starting from the node given as argument.
	 * The nodeStack represents the lists of nodes that have already participated in the traversal.
	 * If a cycle is found it represents the nodes that participate in the cycle.
	 * @param node to be analyzed for a cycle
	 * @param nodeStack list of nodes that have already been traversed in depth first order
	 * @param visited map of nodes that have been analyzed already
	 * @param cycles list of cycles that have been found
	 */
	private void depthFirstTraversal(Node node, List<Node> nodeStack, Map<Node, Boolean> visited, List<List<Node>> cycles) {
		if(visited.containsKey(node)) {
			return // node has already been analyzed
		}
		visited[node] = true
		nodeStack.push(node)
		for(Node childNode : node.outgoing*.toNode) {
			Integer cycleStartIndex = nodeStack.findIndexOf { it == childNode }
			if(cycleStartIndex>=0) {
				// cycle found
				List<Node> cycle = nodeStack[cycleStartIndex..-1] + childNode
				cycles << cycle
			} else {
				depthFirstTraversal(childNode, nodeStack, visited, cycles)
			}

		}
		nodeStack.pop()
	}
}
package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DepcheckUtils
import com.infinit.gradle.depcheck.internal.DependencyGraph
import com.infinit.gradle.depcheck.internal.Edge
import com.infinit.gradle.depcheck.internal.Node
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

/**
 * This gradle task checks if there are nodes that have redundant dependencies.
 * A dependency is redundant if the toNode of the associated dependency graph edge can also be reached
 * via a chain of dependencies all having the same project and configuration as the edge
 * in question starting from a sibling edge.
 * Sibling edges are edges that share the same fromNode with the edge in question.
 * The redundant nodes are issued with a warning.
 *
 * User: owahlen
 * Date: 07.08.13
 */
class DepcheckRedundantTask extends DefaultTask {

	PrintStream out = System.err

	@TaskAction
	def depcheckRedundant() {
		DependencyGraph dependencyGraph = DepcheckUtils.getOrCreateDependencyGraph(project)
		checkForRedundantDependencies(dependencyGraph)
	}

	/**
	 * Check the dependency graph for redundant edges and print them with a warning
	 * @param dependencyGraph of artifacts
	 */
	private void checkForRedundantDependencies(DependencyGraph dependencyGraph) {
		Map<Edge, Map<Node, Boolean>> dominatedNodes = createDominatedNodesMap(dependencyGraph)
		List<Edge> redundantEdges = findRedundantDependencies(dominatedNodes)
		for (Edge edge : redundantEdges) {
			logger.warn("redundant edge found: '${edge.toDetailedString()}'")
		}
	}

	/**
	 * Each edge of the dependency graph is mapped onto a map of nodes.
	 * The nodes in this maps are those that are dominated by the toNode of the edge or the toNode itself.
	 * @param dependencyGraph of artifacts
	 * @return map of each existing edge on a map of nodes. The nodes are dominated by the edge's toNode or the toNode itself.
	 */
	private Map<Edge, Map<Node, Boolean>> createDominatedNodesMap(DependencyGraph dependencyGraph) {
		Map<Edge, Map<Node, Boolean>> dominatedNodes = [:]
		List<Edge> allEdges = dependencyGraph.incomingEdges.values().flatten()
		for (Edge edge : allEdges) {
			// iterate over all edges but skip those that have already been processed previously
			if (dominatedNodes.containsKey(edge)) {
				continue
			}
			dominatedNodes[edge] = [:]
			populateDominatedNodesOfEdges(dominatedNodes, [edge])
		}
		return dominatedNodes
	}

	/**
	 * Recursively collect all nodes that are dominated by the toNode of the edge.
	 * Only those nodes are collected that are connected with a chain of edges with the same project and configuration
	 * as the edge provided as parameter. The toNode of the edge is added to the collection, too.
	 * In order to identify cycles the method also tracks the fromNode of the edge.
	 * @param dominated map of each existing edge on a map of nodes. The nodes are dominated by the edge's toNode or the toNode itself.
	 * @param edgeStack is pushed to or popped from when entering or returning from a depth first traversal of the graph
	 * @param startNode is the fromNode of the edge used in the top level call of the recursion
	 */
	private void populateDominatedNodesOfEdges(Map<Edge, Map<Node, Boolean>> dominated, List<Edge> edgeStack, Node startNode = edgeStack.first().fromNode) {
		Edge edge = edgeStack.last()
		Node toNode = edge.toNode
		if (toNode == startNode || dominated[edgeStack.first()].containsKey(toNode)) {
			return
		}
		edgeStack.each { dominated[it][toNode] = true }

		List<Edge> childEdges = toNode.outgoing.findAll { it.projectName == edge.projectName && it.configurationName == edge.configurationName }
		for (Edge childEdge : childEdges) {
			if (dominated.containsKey(childEdge)) {
				// the nodes dominated by the child edge have already been analyzed.
				// They are simply added to all parent edges from the edgeStack.
				edgeStack.each { dominated[it] << dominated[childEdge] }
			} else {
				// the child edge has not yet been analyzed. It is pushed onto the edgeStack and recursively analyzed.
				dominated[childEdge] = [:]
				edgeStack.push(childEdge)
				populateDominatedNodesOfEdges(dominated, edgeStack, startNode)
				edgeStack.pop()
			}
		}
	}

	/**
	 * Find edges that have an alternative dependency chain from the fromNode to the toNode
	 * @param dominatedNodes maps each edge to a map of nodes. The nodes are dominated by the edge's fromNode.
	 * @return list of edges in the dependencyGraph that are redundant
	 */
	private List<Edge> findRedundantDependencies(Map<Edge, Map<Node, Boolean>> dominatedNodes) {
		Map<String, Edge> redundantEdges = [:]
		for (Edge edge : dominatedNodes.keySet()) {
			if (edge.fromNode.outgoing.any { !it.is(edge) && it.projectName == edge.projectName && it.configurationName == edge.configurationName && dominatedNodes[it].containsKey(edge.toNode) }) {
				redundantEdges[edge.toDetailedString()] = edge
			}
		}
		return redundantEdges.values() as List<Edge>
	}

}
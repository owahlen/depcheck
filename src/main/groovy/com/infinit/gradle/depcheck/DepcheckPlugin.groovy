package com.infinit.gradle.depcheck

import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.Plugin

/**
 * This class represents the plugin and performs all initializations.
 * Specifically it registers tasks with the project the plugin is applied from.
 *
 * User: owahlen
 * Date: 28.07.13
 */
class DepcheckPlugin implements Plugin<Project> {

	public final static String DEPCHECK_PLUGIN_EXTENSION = 'depcheck'

	/**
	 * When the plugin is applied to a project all plugin tasks are registered with it.
	 * @param project the plugin is applied for
	 */
	void apply(Project project) {

        // Add the 'depcheck' extension object
        project.extensions.create(DEPCHECK_PLUGIN_EXTENSION, DepcheckPluginExtension)

		// register tasks with the project
		project.task('depcheck', dependsOn: ['depcheckConflicts', 'depcheckRedundant', 'depcheckCycles'])
		project.task('depcheckConflicts', type: DepcheckConflictsTask)
		project.task('depcheckRedundant', type: DepcheckRedundantTask)
		project.task('depcheckCycles', type: DepcheckCyclesTask)
		project.task('createDependencyDot', type: CreateDependencyDotTask)
        project.task('createDependencyXml', type: CreateDependencyXmlTask)
        project.task('diffDependencyXml', type: DiffDependencyXmlTask)
	}

}

package com.infinit.gradle.depcheck

import com.infinit.gradle.depcheck.internal.DependencyGraph

/**
 * The extension class holds configuration variables passed from the project to the plugin.
 * It is also the container of the dependencyGraph
 *
 * User: owahlen
 * Date: 28.07.13
 */
class DepcheckPluginExtension {

    // reference to the calculated dependency graph
    DependencyGraph dependencyGraph

    // filter the graph analysis on artifacts whose group name starts with includeRegexp
    String includeRegexp

    // define the substring of the artifact version that identifies a breaking change
    String conflictingVersionRegexp

    // only consider configurations that match this regular expression
    // relevant for: DepcheckConflictsTask, DepcheckCyclesTask, DepcheckRedundantTask
    String configurationRegexp

    // only consider configurations for diffDependencyXml that match this regular expression
    // relevant for: DiffDependencyXmlTask
    String configurationDiffRegexp

    // in the output of DiffDependencyXmlTask hide the module column
    Boolean hideModuleInDiff

    // default file path to write the xml dependency tree into relative to the root project
    String dependencyXml

    // default file path relative to root project to write the dependency diff file to
    String dependencyDiff
}

package com.infinit.gradle.depcheck.internal

/**
 * An edge of the dependency graph represents a dependency between artifacts.
 * The edge is directed and points to the artifact that the source artifact depends on.
 * I.e. the edge direction indicates the "depends on".
 *
 * User: owahlen
 * Date: 28.07.13
 */
class Edge {
	Node fromNode
	Node toNode
	String projectName
	String configurationName

	/**
	 * The project configuration is a String with the format "<project name>#<configuration name>".
	 * The configuration is a gradle configuration (e.g. 'compile', 'runtime')
	 * @return the project configuration string
	 */
	String getProjectConfiguration() {
		return "$projectName#$configurationName"
	}

	/**
	 * The string representation of an edge has the format "<group>:<name>:<version>-><group>:<name>:<version>".
	 * @return string representation of the edge
	 */
	@Override
	String toString() {
		return "$fromNode->$toNode"
	}

	/**
	 * The string representation of an edge has the format "<project>:<configuration> <group>:<name>:<version>-><group>:<name>:<version>".
	 * @return string representation of the edge
	 */
	String toDetailedString() {
		return "$projectName:$configurationName $fromNode->$toNode"
	}

	/**
	 * Two edges are equal if nodes, projectName and configurationName are the same.
	 * @param other edge to compare with
	 * @return true if equal, false otherwise
	 */
	@Override
	boolean equals(Object other) {
		if (other == null) return false
		if (this.is(other)) return true
		if (!(other instanceof Edge)) return false
		if (fromNode != ((Edge) other).fromNode) return false
		if (toNode != ((Edge) other).toNode) return false
		if (projectName != ((Edge) other).projectName) return false
		if (configurationName != ((Edge) other).configurationName) return false
		return true
	}
}

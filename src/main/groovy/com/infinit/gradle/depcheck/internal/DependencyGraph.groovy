package com.infinit.gradle.depcheck.internal

import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration
import org.gradle.api.artifacts.result.ResolvedDependencyResult
import org.gradle.api.artifacts.result.ResolvedModuleVersionResult

/**
 * Represents the dependency graph between all artifacts of the project.
 *
 * User: owahlen
 * Date: 28.07.13
 */
@Slf4j
class DependencyGraph {

    // *** Mandatory parameters needed for population of the graph ***

    // rootProject the DependencyGraph shall be constructed for
    Project rootProject

    // if includeRegexp is not null only add nodes to the graph whose moduleVersion matches the includeRegexp
    String includeRegexp

    // if configurationRegexp is not null only add nodes to the graph that participate in a matching configuration
    String configurationRegexp

    // *** Helper Variables ***

    // lookup map from full artifact name ("<group>:<name>:<version>") onto the node in the dependency tree
    Map<String, Node> moduleVersions = [:]

    // lookup map from artifact name without version ("<group>:<name>") onto matching nodes in dependency tree
    Map<String, List<Node>> modules = [:]

    // lookup map of incoming edges for nodes. The key has the format "<project>#<configuration>#<group>:<name>:<version>"
    Map<String, List<Edge>> incomingEdges = [:]

    /**
     * Populate the DependencyGraph for the project and all its sub-projects.
     */
    DependencyGraph populate() {
        assert rootProject
        Integer totalProjects = rootProject.allprojects.size()
        rootProject.allprojects.eachWithIndex { Project project, Integer index ->
            log.info("Analyzing project dependencies (${(int) ((index + 1) * 100) / totalProjects}%): '$project.name'")
            getProjectConfigurations(project).each { Configuration configuration ->
                // todo: Gradle 1.6 stores resolved configurations in a way that can cause heap space exceptions on big projects.
                // As a workaround the configuration is duplicated, the duplicate is resolved and then released from memory.
                // This reduces performance and should be removed when the problem is fixed in Gradle 1.7
                Configuration configurationCopy = configuration.copyRecursive()
                ResolvedModuleVersionResult root = configurationCopy.incoming.resolutionResult.root
                populateDependencyGraph(root, project.name, configuration.name)
            }
        }
        return this
    }

    /**
     * Add a Node to the graph if it does not yet exist.
     * @param node to be added to the graph
     * @return if the node to be added did not yet exist it is returned. Otherwise the existing node is returned.
     */
    Node addOrLookupNode(Node node) {
        String nodeString = node.toString()
        Node existingNode = moduleVersions[nodeString]
        if (existingNode) {
            // node has already been added
            return existingNode
        }
        // store node in moduleVersions
        moduleVersions[nodeString] = node

        // store node in modules
        List<Node> modulesList = modules[node.moduleString]
        if (!modulesList) {
            modulesList = []
            modules[node.moduleString] = modulesList
        }
        modulesList << node
        return node
    }

    /**
     * Add the edge to the lookup maps an register the edge with the fromNode and toNode
     * @param edge to be added
     */
    void addEdge(Edge edge) {
        edge.fromNode.outgoing << edge
        edge.toNode.incoming << edge
        String incomingEdgeString = "$edge.projectConfiguration#$edge.toNode"
        List<Edge> edges = incomingEdges[incomingEdgeString]
        if (!edges) {
            edges = []
            incomingEdges[incomingEdgeString] = edges
        }
        edges << edge
    }

    private Set<Configuration> getProjectConfigurations(Project project) {
        if (configurationRegexp) {
            return project.configurations.findAll { it.name =~ configurationRegexp }
        } else {
            return project.configurations
        }
    }

    /**
     * Construct a dependency graph with Node and Edge class instances.
     * The Node instances represent artifacts and the Edges dependencies between the artifacts.
     * Note that the graph is directed and potentially cyclic.
     * @param resolved The ResolvedModuleVersionResult that is the result of gradle's dependency resolution
     * @param projectName name of the project that is currently being processed
     * @param configurationName name of the configuration that is currently being processed
     * @param visitedInProjectConfiguration remember nodes that have been visited already in order to stop recursion
     * @param parent node that depends on this node
     */
    private void populateDependencyGraph(ResolvedModuleVersionResult resolved, String projectName, String configurationName, Map<Node, Boolean> visitedInProjectConfiguration = [:], Node parent = null) {

        Node newNode = new Node(group: resolved.id.group, name: resolved.id.name, version: resolved.id.version)
        if (includeRegexp && !newNode.toString().matches(includeRegexp)) {
            // the resolved dependency is not added to the graph and recursion stops
            return
        }

        Node node = addOrLookupNode(newNode)

        if (parent) {
            // Check if we have to add an edge from the parent to this node
            List<Edge> incomingEdges = node.incoming.findAll { it.projectConfiguration == "$projectName#$configurationName" }
            if (!incomingEdges.collect { it.fromNode }.contains(parent)) {
                addEdge(new Edge(fromNode: parent, toNode: node, projectName: projectName, configurationName: configurationName))
            }
        }

        if (visitedInProjectConfiguration.containsKey(node)) {
            // The node has already been processed in the current project configuration. Therefore the recursion ends.
            return
        }
        visitedInProjectConfiguration[node] = true

        // analyze child dependencies
        resolved.dependencies.each { ResolvedDependencyResult dependency ->
            if (!dependency.requested.matchesStrictly(dependency.selected.id)) {
                // gradle has modified the selected dependency version due to conflicts, forced changes or selection rules.
                // In this case the requested version is put into the graph to mark the difference but it is not further analyzed.
                Node newRequestedNode = new Node(group: dependency.requested.group, name: dependency.requested.name, version: dependency.requested.version)
                if (!includeRegexp || newRequestedNode.toString().matches(includeRegexp)) {
                    Node requestedNode = addOrLookupNode(newRequestedNode)
                    addEdge(new Edge(fromNode: node, toNode: requestedNode, projectName: projectName, configurationName: configurationName))
                }
            } else {
                Node newSelectedNode = new Node(group: dependency.selected.id.group, name: dependency.selected.id.name, version: dependency.selected.id.version)
                if (!includeRegexp || newSelectedNode.toString().matches(includeRegexp)) {
                    // recursion
                    populateDependencyGraph(dependency.selected, projectName, configurationName, visitedInProjectConfiguration, node)
                }
            }
        }

    }
}

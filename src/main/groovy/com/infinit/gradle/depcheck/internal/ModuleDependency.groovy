package com.infinit.gradle.depcheck.internal

/**
 * Module Dependency Holder
 *
 * User: owahlen
 * Date: 22.10.13
 */
class ModuleDependency implements Comparable<ModuleDependency> {
    String projectName
    String configurationName
    String group
    String module

    String previousVersion
    String currentVersion

    /**
     * Get a String representation of the vector project, configuration, group and module
     * @return String representing project, configuration, group and module
     */
    String getProjectModule() {
        return "$projectName:$configurationName:$group:$module"
    }

    /**
     * Compare a ModuleDependency to another using its String representation.
     * This method is used for checking uniqueness of two ModuleDependencies
     * @param o the other ModuleDependency to compareTo
     * @return the comparison of the String representations of the two ModuleDependencies
     */
    @Override
    int compareTo(ModuleDependency o) {
        return this.toString() <=> o.toString()
    }

    /**
     * Return a String representation of all properties of instance
     * @return String representation of all properties of the instance
     */
    @Override
    String toString() {
        return "$projectName:$configurationName:$group:$module:$previousVersion:$currentVersion"
    }

}

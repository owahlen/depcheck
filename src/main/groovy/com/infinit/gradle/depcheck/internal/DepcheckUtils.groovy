package com.infinit.gradle.depcheck.internal

import com.infinit.gradle.depcheck.DepcheckPlugin
import com.infinit.gradle.depcheck.DepcheckPluginExtension
import org.gradle.api.Project

/**
 * Utility methods of the plugin
 *
 * User: owahlen
 * Date: 28.07.13
 */
class DepcheckUtils {

    /**
     * The method tries to find a DependencyGraph in the project's extra properties.
     * If none is found it is computed and stored in the project's extra properties.
     * @param project the DependencyGraph is associated with
     * @return DependencyGraph
     */
    static DependencyGraph getOrCreateDependencyGraph(Project project) {
        DepcheckPluginExtension extension = getExtension(project)
        if (!extension.dependencyGraph) {
            // generate the dependency graph
            extension.dependencyGraph = new DependencyGraph(
                    rootProject: project,
                    includeRegexp: extension.includeRegexp,
                    configurationRegexp: extension.configurationRegexp).populate()
        }
        return extension.dependencyGraph
    }

    static DepcheckPluginExtension getExtension(Project project) {
        String extensionName = DepcheckPlugin.DEPCHECK_PLUGIN_EXTENSION
        DepcheckPluginExtension extension = (DepcheckPluginExtension) project.extensions.findByName(extensionName)
        if(!extension) {
            extension = (DepcheckPluginExtension) project.rootProject.extensions.getByName(extensionName)
        }
        return extension
    }

}

package com.infinit.gradle.depcheck.internal

/**
 * A node in the dependency graph represents an artifact.
 * Artifacts that this artifact is dependent on are represented by outgoing edges.
 * Incoming edges represent artifacts that depend on this artifact.
 *
 * User: owahlen
 * Date: 28.07.13
 */
class Node {
	String group
	String name
	String version
	List<Edge> incoming = []
	List<Edge> outgoing = []

	/**
	 * The string representation of a node has the format "<group>:<name>:<version>".
	 * @return string representation of the node
	 */
	@Override
	String toString() {
		return "$group:$name:$version"
	}

	/**
	 * The module string has the format "<group>:<name>".
	 * Thus it represents a set of artifacts that share the same group and name but may have different versions.
	 * @return the module string
	 */
	String getModuleString() {
		return "$group:$name"
	}

	/**
	 * Two nodes are equal if group, name, and version are the same.
	 * @param other node to compare with
	 * @return true if equal, false otherwise
	 */
	@Override
	boolean equals(Object other) {
		if (other == null) return false
		if (this.is(other)) return true
		if (!(other instanceof Node)) return false
		if (group != ((Node) other).group) return false
		if (name != ((Node) other).name) return false
		if (version != ((Node) other).version) return false
		return true
	}
}
